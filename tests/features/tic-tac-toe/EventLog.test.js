import React from 'react';
import { shallow } from 'enzyme';
import { EventLog } from '../../../src/features/tic-tac-toe/EventLog';

describe('tic-tac-toe/EventLog', () => {
  it('renders node with correct class name', () => {
    const props = {
      ticTacToe: { events: [] },
      actions: {},
    };
    const eventLog = shallow(<EventLog {...props} />);

    expect(eventLog.find('.tic-tac-toe-event-log').length).toBe(1);
  });
});
