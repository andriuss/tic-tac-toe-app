import React from 'react';
import { shallow } from 'enzyme';
import { Game } from '../../../src/features/tic-tac-toe/Game';

describe('tic-tac-toe/Game', () => {
  it('renders node with correct class name', () => {
    const props = {
      ticTacToe: {},
      actions: {},
    };
    const game = shallow(<Game {...props} />);

    expect(game.find('.tic-tac-toe-game').length).toBe(1);
  });
});
