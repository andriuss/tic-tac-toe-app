import React from 'react';
import { shallow } from 'enzyme';
import { Board } from '../../../src/features/tic-tac-toe/Board';

describe('tic-tac-toe/Board', () => {
  const props = {
    ticTacToe: { events: [] },
    actions: {},
    board: [['','',''],['','',''],['','','']]
  };

  it('renders node with correct class name', () => {
    const bpard = shallow(<Board {...props} />);

    expect(bpard.find('.tic-tac-toe-board').length).toBe(1);
  });

  it('renders initial empty board', () => {
    const board = shallow(<Board {...props} />);

    expect(board.find('.board .cell.empty').length).toBe(9);
    board.find('.board .cell').forEach((node) => {
      expect(node.text()).toBe('');
    });
  });
});
