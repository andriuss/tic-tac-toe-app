import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';
import { PLAYER1 } from '../../../../src/features/tic-tac-toe/constants';

import {
  TIC_TAC_TOE_ADD_EVENT_BEGIN,
  TIC_TAC_TOE_ADD_EVENT_SUCCESS,
  TIC_TAC_TOE_ADD_EVENT_FAILURE,
  TIC_TAC_TOE_ADD_EVENT_DISMISS_ERROR,
} from '../../../../src/features/tic-tac-toe/redux/constants';

import {
  addEvent,
  dismissAddEventError,
  reducer,
} from '../../../../src/features/tic-tac-toe/redux/addEvent';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('tic-tac-toe/redux/addEvent', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  beforeEach(() => {
    nock('http://localhost:3000')
      .post('/events')
      .reply(200, []);
  })

  it('dispatches success action when addEvent succeeds', () => {
    const store = mockStore({});

    return store.dispatch(addEvent())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', TIC_TAC_TOE_ADD_EVENT_BEGIN);
        expect(actions[1]).toHaveProperty('type', TIC_TAC_TOE_ADD_EVENT_SUCCESS);
      });
  });

  it('dispatches failure action when addEvent fails', () => {
    const store = mockStore({});

    return store.dispatch(addEvent({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', TIC_TAC_TOE_ADD_EVENT_BEGIN);
        expect(actions[1]).toHaveProperty('type', TIC_TAC_TOE_ADD_EVENT_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissAddEventError', () => {
    const expectedAction = {
      type: TIC_TAC_TOE_ADD_EVENT_DISMISS_ERROR,
    };
    expect(dismissAddEventError()).toEqual(expectedAction);
  });

  it('handles action type TIC_TAC_TOE_ADD_EVENT_BEGIN correctly', () => {
    const prevState = { addEventPending: false, events: []  };
    const event = { player: PLAYER1 };
    const state = reducer(
      prevState,
      { type: TIC_TAC_TOE_ADD_EVENT_BEGIN, event }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.addEventPending).toBe(true);
    expect(state.events).toEqual([ event ]);
  });

  it('handles action type TIC_TAC_TOE_ADD_EVENT_SUCCESS correctly', () => {
    const prevState = { addEventPending: true };
    const state = reducer(
      prevState,
      { type: TIC_TAC_TOE_ADD_EVENT_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.addEventPending).toBe(false);
  });

  it('handles action type TIC_TAC_TOE_ADD_EVENT_FAILURE correctly', () => {
    const prevState = { addEventPending: true };
    const state = reducer(
      prevState,
      { type: TIC_TAC_TOE_ADD_EVENT_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.addEventPending).toBe(false);
    expect(state.addEventError).toEqual(expect.anything());
  });

  it('handles action type TIC_TAC_TOE_ADD_EVENT_DISMISS_ERROR correctly', () => {
    const prevState = { addEventError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: TIC_TAC_TOE_ADD_EVENT_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.addEventError).toBe(null);
  });
});

