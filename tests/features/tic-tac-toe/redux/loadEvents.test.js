import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';
import { PLAYER1, PLAYER2 } from '../../../../src/features/tic-tac-toe/constants';

import {
  TIC_TAC_TOE_LOAD_EVENTS_BEGIN,
  TIC_TAC_TOE_LOAD_EVENTS_SUCCESS,
  TIC_TAC_TOE_LOAD_EVENTS_FAILURE,
  TIC_TAC_TOE_LOAD_EVENTS_DISMISS_ERROR,
} from '../../../../src/features/tic-tac-toe/redux/constants';

import {
  loadEvents,
  dismissLoadEventsError,
  reducer,
} from '../../../../src/features/tic-tac-toe/redux/loadEvents';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('tic-tac-toe/redux/loadEvents', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  const events = [
    { player: PLAYER1, date: new Date() },
    { player: PLAYER2, date: new Date() },
  ];

  beforeEach(() => {
    nock('http://localhost:3000')
      .get('/events')
      .reply(200, [events]);
  })

  it('dispatches success action when loadEvents succeeds', () => {
    const store = mockStore({});

    return store.dispatch(loadEvents())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', TIC_TAC_TOE_LOAD_EVENTS_BEGIN);
        expect(actions[1]).toHaveProperty('type', TIC_TAC_TOE_LOAD_EVENTS_SUCCESS);
        expect(actions[1]).toHaveProperty('data.data', [events.map(e => ({...e, date: e.date.toISOString()}))]);
      });
  });

  it('dispatches failure action when loadEvents fails', () => {
    const store = mockStore({});

    return store.dispatch(loadEvents({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', TIC_TAC_TOE_LOAD_EVENTS_BEGIN);
        expect(actions[1]).toHaveProperty('type', TIC_TAC_TOE_LOAD_EVENTS_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissLoadEventsError', () => {
    const expectedAction = {
      type: TIC_TAC_TOE_LOAD_EVENTS_DISMISS_ERROR,
    };
    expect(dismissLoadEventsError()).toEqual(expectedAction);
  });

  it('handles action type TIC_TAC_TOE_LOAD_EVENTS_BEGIN correctly', () => {
    const prevState = { loadEventsPending: false };
    const state = reducer(
      prevState,
      { type: TIC_TAC_TOE_LOAD_EVENTS_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.loadEventsPending).toBe(true);
  });

  it('handles action type TIC_TAC_TOE_LOAD_EVENTS_SUCCESS correctly', () => {
    const prevState = { loadEventsPending: true };
    const state = reducer(
      prevState,
      { type: TIC_TAC_TOE_LOAD_EVENTS_SUCCESS, data: { data: events } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.loadEventsPending).toBe(false);
    expect(state.events).toEqual(events);
  });

  it('handles action type TIC_TAC_TOE_LOAD_EVENTS_FAILURE correctly', () => {
    const prevState = { loadEventsPending: true };
    const state = reducer(
      prevState,
      { type: TIC_TAC_TOE_LOAD_EVENTS_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.loadEventsPending).toBe(false);
    expect(state.loadEventsError).toEqual(expect.anything());
  });

  it('handles action type TIC_TAC_TOE_LOAD_EVENTS_DISMISS_ERROR correctly', () => {
    const prevState = { loadEventsError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: TIC_TAC_TOE_LOAD_EVENTS_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.loadEventsError).toBe(null);
  });
});

