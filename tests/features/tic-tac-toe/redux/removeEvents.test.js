import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';
import { PLAYER1 } from '../../../../src/features/tic-tac-toe/constants';

import {
  TIC_TAC_TOE_REMOVE_EVENTS_BEGIN,
  TIC_TAC_TOE_REMOVE_EVENTS_SUCCESS,
  TIC_TAC_TOE_REMOVE_EVENTS_FAILURE,
  TIC_TAC_TOE_REMOVE_EVENTS_DISMISS_ERROR,
} from '../../../../src/features/tic-tac-toe/redux/constants';

import {
  removeEvents,
  dismissRemoveEventsError,
  reducer,
} from '../../../../src/features/tic-tac-toe/redux/removeEvents';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('tic-tac-toe/redux/removeEvents', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  beforeEach(() => {
    nock('http://localhost:3000')
      .delete('/events')
      .reply(200, []);
  })

  it('dispatches success action when removeEvents succeeds', () => {
    const store = mockStore({});

    return store.dispatch(removeEvents())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', TIC_TAC_TOE_REMOVE_EVENTS_BEGIN);
        expect(actions[1]).toHaveProperty('type', TIC_TAC_TOE_REMOVE_EVENTS_SUCCESS);
      });
  });

  it('dispatches failure action when removeEvents fails', () => {
    const store = mockStore({});

    return store.dispatch(removeEvents({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', TIC_TAC_TOE_REMOVE_EVENTS_BEGIN);
        expect(actions[1]).toHaveProperty('type', TIC_TAC_TOE_REMOVE_EVENTS_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissRemoveEventsError', () => {
    const expectedAction = {
      type: TIC_TAC_TOE_REMOVE_EVENTS_DISMISS_ERROR,
    };
    expect(dismissRemoveEventsError()).toEqual(expectedAction);
  });

  it('handles action type TIC_TAC_TOE_REMOVE_EVENTS_BEGIN correctly', () => {
    const prevState = { removeEventsPending: false, events: [ { player: PLAYER1 }] };
    const state = reducer(
      prevState,
      { type: TIC_TAC_TOE_REMOVE_EVENTS_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.removeEventsPending).toBe(true);
    expect(state.events).toEqual([]);
  });

  it('handles action type TIC_TAC_TOE_REMOVE_EVENTS_SUCCESS correctly', () => {
    const prevState = { removeEventsPending: true };
    const state = reducer(
      prevState,
      { type: TIC_TAC_TOE_REMOVE_EVENTS_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.removeEventsPending).toBe(false);
  });

  it('handles action type TIC_TAC_TOE_REMOVE_EVENTS_FAILURE correctly', () => {
    const prevState = { removeEventsPending: true };
    const state = reducer(
      prevState,
      { type: TIC_TAC_TOE_REMOVE_EVENTS_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.removeEventsPending).toBe(false);
    expect(state.removeEventsError).toEqual(expect.anything());
  });

  it('handles action type TIC_TAC_TOE_REMOVE_EVENTS_DISMISS_ERROR correctly', () => {
    const prevState = { removeEventsError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: TIC_TAC_TOE_REMOVE_EVENTS_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.removeEventsError).toBe(null);
  });
});

