import findWinner from '../../../src/features/tic-tac-toe/winner-service';
import { PLAYER1, PLAYER2, TIE } from '../../../src/features/tic-tac-toe/constants';

describe('winner service', () => {
  const aBoard = (modBoardFn = x => x) => {
    const board = [
      ['', '', ''],
      ['', '', ''],
      ['', '', ''],
    ]
    modBoardFn(board);
    return board;
  }

  it('return empty', () => {
    expect(findWinner(aBoard())).toBe(undefined);
  });

  it('player1 wins in row', () => {
    expect(findWinner(aBoard(
      board => {
        board[0][0] = PLAYER1
        board[0][1] = PLAYER1
        board[0][2] = PLAYER1
      }
    ))).toBe(PLAYER1);
  });

  it('player2 wins in row', () => {
    expect(findWinner(aBoard(
      board => {
        board[1][0] = PLAYER2
        board[1][1] = PLAYER2
        board[1][2] = PLAYER2
      }
    ))).toBe(PLAYER2);
  });

  it('player1 wins in column', () => {
    expect(findWinner(aBoard(
      board => {
        board[0][0] = PLAYER1
        board[1][0] = PLAYER1
        board[2][0] = PLAYER1
      }
    ))).toBe(PLAYER1);

  });

  it('player2 wins in column', () => {
    expect(findWinner(aBoard(
      board => {
        board[0][1] = PLAYER2
        board[1][1] = PLAYER2
        board[2][1] = PLAYER2
      }
    ))).toBe(PLAYER2);
  });

  it('player1 wins in diagonal', () => {
    expect(findWinner(aBoard(
      board => {
        board[0][0] = PLAYER1
        board[1][1] = PLAYER1
        board[2][2] = PLAYER1
      }
    ))).toBe(PLAYER1);
  });

  it('player2 wins in diagonal', () => {
    expect(findWinner(aBoard(
      board => {
        board[0][2] = PLAYER2
        board[1][1] = PLAYER2
        board[2][0] = PLAYER2
      }
    ))).toBe(PLAYER2);
  });

  it('tie game', () => {
    expect(findWinner(aBoard(
      board => {
        board[0][0] = PLAYER1
        board[0][1] = PLAYER2
        board[0][2] = PLAYER1
        board[1][0] = PLAYER1
        board[1][1] = PLAYER2
        board[1][2] = PLAYER2
        board[2][0] = PLAYER2
        board[2][1] = PLAYER1
        board[2][2] = PLAYER1
      }
    ))).toBe(TIE);
  });
});
