context('tic tac toe', () => {

  afterEach(() => cy.get('.restart').click());

  const clickCellAt = index => {
    cy.get('.board div .cell')
      .eq(index).click()
  };

  const home = 'http://localhost:8080';

  it('player 1 wins', () => {
    cy.visit(home);

    clickCellAt(1);
    clickCellAt(2);
    clickCellAt(4);
    clickCellAt(6);
    clickCellAt(7);

    cy.get('.status-row.winner.name')
      .should('contain', 'Player 1');
  })

  it('player 2 wins', () => {
    cy.visit(home);

    clickCellAt(0);
    clickCellAt(4);
    clickCellAt(8);
    clickCellAt(6);
    clickCellAt(1);
    clickCellAt(2);

    cy.get('.status-row.winner.name')
      .should('contain', 'Player 2');
  })

  it('tie game', () => {
    cy.visit(home);

    clickCellAt(0);
    clickCellAt(1);
    clickCellAt(8);
    clickCellAt(4);
    clickCellAt(7);
    clickCellAt(6);
    clickCellAt(2);
    clickCellAt(5);
    clickCellAt(3);

    cy.get('.status-row.winner.name')
      .should('contain', 'Tie');
  })

  it('reset game', () => {
    cy.visit(home);

    clickCellAt(0);
    cy.get('.restart').click();

    cy.get('.status-row').eq(1)
      .should('contain', 'Player 1');

    cy.get('.board div .cell').eq(0)
      .should('contain', '');
  })

  it('shows event log', () => {
    cy.visit(home);

    clickCellAt(4);
    clickCellAt(5);

    cy.get('.events > :nth-child(1) > div')
      .should('contain', 'Player 1 [1]:[1] X');

    cy.get('.events > :nth-child(2) > div')
      .should('contain', 'Player 2 [1]:[2] O');
  })
})