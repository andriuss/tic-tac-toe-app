import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import { NAME, PLAYER1, PLAYER2 } from './constants';
import findWinner from './winner-service';

export class Board extends Component {
  static propTypes = {
    ticTacToe: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  makeMove = (row, cell) => {
    if (this.props.board[row][cell] !== '' || this.props.winner !== undefined) {
      return;
    }
    const player = this.props.currentPlayer;
    this.props.actions.addEvent({ player, move: { row, cell }, date: new Date() });
  };

  componentDidMount = () => {
    this.props.actions.loadEvents();
  }

  renderOnlineStatus = () => {
    const status = this.props.ticTacToe.loadEventsError ? 'offline' : 'online';
    const text = status === 'online' ? 'Online': 'Offline';
    return <a className={status} href="http://localhost:3000" target="blank">[{text}]</a>;
  };

  render() {
    return (
      <div className="tic-tac-toe-board">
        <h3 className="title">Tic Tac Toe {this.renderOnlineStatus()}</h3>
        <div className={classNames('board', {[`finished`]: this.props.winner})}>
          {this.props.board.map((row, rowIdx) => {
            return (
              <div key={rowIdx} className="row">
                {row.map((cell, cellIdx) => {
                  const cellId = rowIdx * 3 + cellIdx;
                  const clickable = cell === '' && !this.props.winner;
                  return (
                    <div
                      key={cellId}
                      className={classNames('cell', clickable && 'empty')}
                      onClick={() => this.makeMove(rowIdx, cellIdx)}
                    >
                      {cell}
                    </div>
                  );
                })}
              </div>
            );
          })}
        </div>
        <div className="status">
          {!this.props.winner && <div className="status-row">Move:</div>}
          {!this.props.winner && (
            <div className="status-row">{NAME[this.props.currentPlayer]}</div>
          )}
          {this.props.winner && <div className="status-row winner">Winner is:</div>}
          {this.props.winner && <div className="status-row winner name">{NAME[this.props.winner]}</div>}
        </div>
        <button className="restart" type="button" onClick={this.props.actions.removeEvents}>Restart</button>
      </div>
    );
  }
}

const initialState = {
  board: [['', '', ''], ['', '', ''], ['', '', '']],
  currentPlayer: PLAYER1,
  winner: undefined,
};

const processEvents = events => {
  let board = [['', '', ''], ['', '', ''], ['', '', '']];
  let { currentPlayer, winner } = initialState;
  for (const event of events) {
    const { player, move: { row, cell } } = event;
    board[row][cell] = player;
    winner = findWinner(board);
    currentPlayer = winner ? player : player === PLAYER1 ? PLAYER2 : PLAYER1;
  }
  return {
    board, currentPlayer, winner,
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    ticTacToe: state.ticTacToe,
    ...processEvents(state.ticTacToe.events)
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Board);
