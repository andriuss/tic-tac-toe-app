// This is the JSON way to define React Router rules in a Rekit app.
// Learn more from: http://rekit.js.org/docs/routing.html

import {
  Board,
  Game,
} from './';

export default {
  path: '/',
  name: 'Tic tac toe',
  childRoutes: [
    { path: 'board', name: 'Board', component: Board },
    { path: 'game', name: 'Game', component: Game, isIndex: true },
  ],
};
