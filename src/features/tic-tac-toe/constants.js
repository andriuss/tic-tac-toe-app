export const PLAYER1 = 'X';
export const PLAYER2 = 'O';
export const TIE = 'TIE';

export const NAME = {
  [PLAYER1]: 'Player 1',
  [PLAYER2]: 'Player 2',
  [TIE]: 'Tie',
}