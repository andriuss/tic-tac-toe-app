import {
  TIC_TAC_TOE_LOAD_EVENTS_BEGIN,
  TIC_TAC_TOE_LOAD_EVENTS_SUCCESS,
  TIC_TAC_TOE_LOAD_EVENTS_FAILURE,
  TIC_TAC_TOE_LOAD_EVENTS_DISMISS_ERROR,
} from './constants';
import axios from '../axios';

export function loadEvents(args = {}) {
  return (dispatch) => {
    dispatch({
      type: TIC_TAC_TOE_LOAD_EVENTS_BEGIN,
    });

    const promise = new Promise((resolve, reject) => {
      const doRequest = axios.get('/events');
      doRequest.then(
        (res) => {
          dispatch({
            type: TIC_TAC_TOE_LOAD_EVENTS_SUCCESS,
            data: res,
          });
          resolve(res);
        },
        (err) => {
          dispatch({
            type: TIC_TAC_TOE_LOAD_EVENTS_FAILURE,
            data: { error: err },
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function dismissLoadEventsError() {
  return {
    type: TIC_TAC_TOE_LOAD_EVENTS_DISMISS_ERROR,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case TIC_TAC_TOE_LOAD_EVENTS_BEGIN:
      return {
        ...state,
        loadEventsPending: true,
        loadEventsError: null,
      };

    case TIC_TAC_TOE_LOAD_EVENTS_SUCCESS:
      return {
        ...state,
        events: action.data.data.map(e => ({...e, date: new Date(e.date) })),
        loadEventsPending: false,
        loadEventsError: null,
      };

    case TIC_TAC_TOE_LOAD_EVENTS_FAILURE:
      return {
        ...state,
        loadEventsPending: false,
        loadEventsError: action.data.error,
      };

    case TIC_TAC_TOE_LOAD_EVENTS_DISMISS_ERROR:
      return {
        ...state,
        loadEventsError: null,
      };

    default:
      return state;
  }
}
