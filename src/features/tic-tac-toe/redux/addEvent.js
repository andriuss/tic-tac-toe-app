import axios from '../axios';
import {
  TIC_TAC_TOE_ADD_EVENT_BEGIN,
  TIC_TAC_TOE_ADD_EVENT_SUCCESS,
  TIC_TAC_TOE_ADD_EVENT_FAILURE,
  TIC_TAC_TOE_ADD_EVENT_DISMISS_ERROR,
} from './constants';

export function addEvent(event = {}) {
  return (dispatch) => {
    dispatch({
      type: TIC_TAC_TOE_ADD_EVENT_BEGIN,
      event,
    });

    const promise = new Promise((resolve, reject) => {
      const doRequest = axios.post('/events', event);

      doRequest.then(
        (res) => {
          dispatch({
            type: TIC_TAC_TOE_ADD_EVENT_SUCCESS,
            data: res,
          });
          resolve(res);
        },
        (err) => {
          dispatch({
            type: TIC_TAC_TOE_ADD_EVENT_FAILURE,
            data: { error: err },
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

// Async action saves request error by default, this method is used to dismiss the error info.
// If you don't want errors to be saved in Redux store, just ignore this method.
export function dismissAddEventError() {
  return {
    type: TIC_TAC_TOE_ADD_EVENT_DISMISS_ERROR,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case TIC_TAC_TOE_ADD_EVENT_BEGIN:
      // Just after a request is sent
      return {
        ...state,
        events: [...state.events, action.event],
        addEventPending: true,
        addEventError: null,
      };

    case TIC_TAC_TOE_ADD_EVENT_SUCCESS:
      // The request is success
      return {
        ...state,
        addEventPending: false,
        addEventError: null,
      };

    case TIC_TAC_TOE_ADD_EVENT_FAILURE:
      // The request is failed
      return {
        ...state,
        addEventPending: false,
        addEventError: action.data.error,
      };

    case TIC_TAC_TOE_ADD_EVENT_DISMISS_ERROR:
      // Dismiss the request failure error
      return {
        ...state,
        addEventError: null,
      };

    default:
      return state;
  }
}
