import axios from '../axios';
import {
  TIC_TAC_TOE_REMOVE_EVENTS_BEGIN,
  TIC_TAC_TOE_REMOVE_EVENTS_SUCCESS,
  TIC_TAC_TOE_REMOVE_EVENTS_FAILURE,
  TIC_TAC_TOE_REMOVE_EVENTS_DISMISS_ERROR,
} from './constants';

export function removeEvents(args = {}) {
  return (dispatch) => {
    dispatch({
      type: TIC_TAC_TOE_REMOVE_EVENTS_BEGIN,
    });

    const promise = new Promise((resolve, reject) => {
      const doRequest = axios.delete('/events');
      doRequest.then(
        (res) => {
          dispatch({
            type: TIC_TAC_TOE_REMOVE_EVENTS_SUCCESS,
            data: res,
          });
          resolve(res);
        },
        (err) => {
          dispatch({
            type: TIC_TAC_TOE_REMOVE_EVENTS_FAILURE,
            data: { error: err },
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function dismissRemoveEventsError() {
  return {
    type: TIC_TAC_TOE_REMOVE_EVENTS_DISMISS_ERROR,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case TIC_TAC_TOE_REMOVE_EVENTS_BEGIN:
      return {
        ...state,
        events: [],
        removeEventsPending: true,
        removeEventsError: null,
      };

    case TIC_TAC_TOE_REMOVE_EVENTS_SUCCESS:
      return {
        ...state,
        removeEventsPending: false,
        removeEventsError: null,
      };

    case TIC_TAC_TOE_REMOVE_EVENTS_FAILURE:
      return {
        ...state,
        removeEventsPending: false,
        removeEventsError: action.data.error,
      };

    case TIC_TAC_TOE_REMOVE_EVENTS_DISMISS_ERROR:
      return {
        ...state,
        removeEventsError: null,
      };

    default:
      return state;
  }
}
