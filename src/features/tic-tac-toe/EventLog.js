import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import { NAME } from './constants';

export class EventLog extends Component {
  static propTypes = {
    ticTacToe: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  render() {
    return (
      <div className="tic-tac-toe-event-log">
        <h3 className="title">Events log:</h3>
        <div className="events">
          {this.props.ticTacToe.events.map(({ move, player, date }, idx) => {
            return (
              <div key={date.toISOString()} className="event">
                <div>
                  {date.toLocaleTimeString()} {NAME[player]} [{move.row}]:[{move.cell}] {player}
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    ticTacToe: state.ticTacToe,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EventLog);
