import { PLAYER1, PLAYER2, TIE } from './constants';

const checkRow = (row, player) => row.every(cell => cell === player);

const checkRows = (board, player) => {
  if (board.some(row => checkRow(row, player))) {
    return PLAYER1;
  }
}

const checkColumns = (board, player) => {
  const rowsToColumns = board.map((row, rowIdx) => row.map((_, cellIdx) => board[cellIdx][rowIdx]))
  return checkRows(rowsToColumns, player);
}

const checkDiagonals = (board, player) => {
  const diagonals = [
    [board[0][0], board[1][1], board[2][2]],
    [board[0][2], board[1][1], board[2][0]],
  ]
  return checkRows(diagonals, player);
}

const isWinner = (board, player) =>
  checkRows(board, player) ||
  checkColumns(board, player) ||
  checkDiagonals(board, player)

const isTie = board => board.every(row => row.every(cell => cell !== ''));

const findWinner = board => {
  if (isWinner(board, PLAYER1)) {
    return PLAYER1;
  }

  if (isWinner(board, PLAYER2)) {
    return PLAYER2;
  }

  if (isTie(board)) {
    return TIE;
  }

  return undefined;

}

export default findWinner;