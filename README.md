## Client app for Tic Tac Toe Game
Project build with react, redux and friends. Seeded with rekit.

App works offline (without API) mode, but events and session won't be saved on server.
To work online start API server - `tic-tac-toe-api` before starting the app.

App source is located in `src/features/tic-tac-toe` folder, tests are in `tests/features/tic-tac-toe`

### Local dev:
1. ```npm install``` install dependencies
2. ```npm start``` start the app
3. app should be open and running on [http://localhost:6075](http://localhost:6075)

Also app can be started using docker (docker must be installed on machine):
1. ```npm run build:docker``` builds docker image
2. ```npm run start:docker``` runs docker image (use ctrl+c to stop)
3. Open app on [http://localhost:8080](http://localhost:8080) or run ```npm run open```

### Available commands
1. ```npm test``` run all tests
2. ```npm run test:e2e``` run E2E tests written with cypress. 
Docker container must be build, but not running as the command will start docker and run the E2E tests.
2. ```npm run build``` builds production version of app
3. ```npm run build:docker``` builds docker image andriuss/tic-tac-toe-app
4. ```npm run start:docker``` starts docker image andriuss/tic-tac-toe-app on port 8080
5. ```npm run open``` opens browser on http://localhost:8080
6. ```cypress:open``` opens cypress dev env
7. ```cypress:run```  run E2E cypress tests located in `cypress/integration` folder

### Enjoy the game 🎮 😃!
 
